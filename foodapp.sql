DROP DATABASE IF EXISTS foodapp ;
CREATE DATABASE IF NOT EXISTS foodapp ;
USE foodapp;

-- major table
DROP TABLE IF EXISTS  foodapp.DINING_HALL ;
CREATE TABLE IF NOT EXISTS foodapp.DINING_HALL (
         Dining_Hall_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
         DH_Name VARCHAR(50),
         Rating ENUM('1', '2', '3', '4', '5')  NOT NULL,
         Hours VARCHAR(50),
         Location VARCHAR(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1001 ;

INSERT INTO foodapp.DINING_HALL VALUES(1, 'Turners Place', 1, 'M-F 11:00 AM - 8:00PM', 'Turners Place');
INSERT INTO foodapp.DINING_HALL VALUES(2, 'Turner\'s Place', 2, 'M-F 11:00 AM - 8:00PM', 'Turner\'s Place');
INSERT INTO foodapp.DINING_HALL VALUES(3, 'Turner\'s Place', 3, 'M-F 11:00 AM - 8:00PM', 'Turner\'s Place');
INSERT INTO foodapp.DINING_HALL VALUES(4, 'Turner\'s Place', 4, 'M-F 11:00 AM - 8:00PM', 'Turner\'s Place');
INSERT INTO foodapp.DINING_HALL VALUES(5, 'Turner\'s Place', 5, 'M-F 11:00 AM - 8:00PM', 'Turner\'s Place');
INSERT INTO foodapp.DINING_HALL VALUES(6, 'Hokie Grill', 1, 'M-F 10:00 AM - 8:00PM', 'Hokie Grill');
INSERT INTO foodapp.DINING_HALL VALUES(7, 'Hokie Grill', 2, 'M-F 10:00 AM - 8:00PM', 'Hokie Grill');
INSERT INTO foodapp.DINING_HALL VALUES(8, 'Hokie Grill', 3, 'M-F 10:00 AM - 8:00PM', 'Hokie Grill');
INSERT INTO foodapp.DINING_HALL VALUES(9, 'Hokie Grill', 4, 'M-F 10:00 AM - 8:00PM', 'Hokie Grill');
INSERT INTO foodapp.DINING_HALL VALUES(10, 'Hokie Grill', 5, 'M-F 10:00 AM - 8:00PM', 'Hokie Grill');
INSERT INTO foodapp.DINING_HALL VALUES(11, 'Owen\'s', 1, 'Mon-Sun 9:00 AM - 9:00 PM', 'Owen\'s');
INSERT INTO foodapp.DINING_HALL VALUES(12, 'Owen\'s', 2, 'Mon-Sun 9:00 AM - 9:00 PM', 'Owen\'s');
INSERT INTO foodapp.DINING_HALL VALUES(13, 'Owen\'s', 3, 'Mon-Sun 9:00 AM - 9:00 PM', 'Owen\'s');
INSERT INTO foodapp.DINING_HALL VALUES(14, 'Owen\'s', 4, 'Mon-Sun 9:00 AM - 9:00 PM', 'Owen\'s');
INSERT INTO foodapp.DINING_HALL VALUES(15, 'Owen\'s', 5, 'Mon-Sun 9:00 AM - 9:00 PM', 'Owen\'s');
INSERT INTO foodapp.DINING_HALL VALUES(16, 'D2', 1, 'Mon-Sun 9:00 AM - 9:00 PM', 'D2');
INSERT INTO foodapp.DINING_HALL VALUES(17, 'D2', 2, 'Mon-Sun 9:00 AM - 9:00 PM', 'D2');
INSERT INTO foodapp.DINING_HALL VALUES(18, 'D2', 3, 'Mon-Sun 9:00 AM - 9:00 PM', 'D2');
INSERT INTO foodapp.DINING_HALL VALUES(19, 'D2', 4, 'Mon-Sun 9:00 AM - 9:00 PM', 'D2');
INSERT INTO foodapp.DINING_HALL VALUES(20, 'D2', 5, 'Mon-Sun 9:00 AM - 9:00 PM', 'D2');

SELECT * FROM foodapp.DINING_HALL;

-- major table, theoretically should only be a table with 5 entries
DROP TABLE IF EXISTS  foodapp.DINING_PLAN ;
CREATE TABLE IF NOT EXISTS foodapp.DINING_PLAN 
(
	DiningPlanID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    MaxAmt INT,
    DPName VARCHAR(100),
    Cost INT
) ENGINE=InnoDB AUTO_INCREMENT=2001 ;

INSERT INTO foodapp.DINING_PLAN VALUES(1, 530, "Premium 1", 500);
INSERT INTO foodapp.DINING_PLAN VALUES(2, 520, "Premium 2", 501);
INSERT INTO foodapp.DINING_PLAN VALUES(3, 525, "Premium 3", 511);
INSERT INTO foodapp.DINING_PLAN VALUES(4, 520, "Premium 4", 521);
INSERT INTO foodapp.DINING_PLAN VALUES(5, 521, "Premium 5", 531);
INSERT INTO foodapp.DINING_PLAN VALUES(6, 460, "Mid Flex 1", 480);
INSERT INTO foodapp.DINING_PLAN VALUES(7, 461, "Mid Flex 2", 481);
INSERT INTO foodapp.DINING_PLAN VALUES(8, 460, "Mid Flex 3", 470);
INSERT INTO foodapp.DINING_PLAN VALUES(9, 459, "Mid Flex 4", 489);
INSERT INTO foodapp.DINING_PLAN VALUES(10, 440, "Mid Flex 5", 460);
INSERT INTO foodapp.DINING_PLAN VALUES(11, 430, "Flex 1", 430);
INSERT INTO foodapp.DINING_PLAN VALUES(12, 431, "Flex 2", 431);
INSERT INTO foodapp.DINING_PLAN VALUES(13, 420, "Flex 1", 425);
INSERT INTO foodapp.DINING_PLAN VALUES(14, 435, "Flex 1", 435);
INSERT INTO foodapp.DINING_PLAN VALUES(15, 410, "Flex 1", 410);
INSERT INTO foodapp.DINING_PLAN VALUES(16, 350, "Dining Dollars 1", 350);
INSERT INTO foodapp.DINING_PLAN VALUES(17, 351, "Dining Dollars 2", 354);
INSERT INTO foodapp.DINING_PLAN VALUES(18, 352, "Dining Dollars 3", 353);
INSERT INTO foodapp.DINING_PLAN VALUES(19, 353, "Dining Dollars 4", 352);
INSERT INTO foodapp.DINING_PLAN VALUES(20, 354, "Dining Dollars 5", 351);

SELECT * FROM foodapp.DINING_PLAN;

-- major table
DROP TABLE IF EXISTS  foodapp.RESTAURANT ;
CREATE TABLE IF NOT EXISTS foodapp.RESTAURANT 
(
	Restaurant_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    RestaurantName VARCHAR(100),
    Hours VARCHAR(100),
    Dining_Hall_ID INT NOT NULL, 
    FOREIGN KEY (Dining_Hall_ID) REFERENCES foodapp.DINING_HALL (Dining_Hall_ID)
    ON UPDATE RESTRICT ON DELETE RESTRICT
    
) ENGINE=InnoDB AUTO_INCREMENT=3001 ;

INSERT INTO foodapp.RESTAURANT VALUES(1, "Origami 1", "M-F 10:00 AM - 6:00 PM", 1);
INSERT INTO foodapp.RESTAURANT VALUES(2, "Origami 2", "M-F 10:00 AM - 6:00 PM", 2);
INSERT INTO foodapp.RESTAURANT VALUES(3, "Origami 3", "M-F 10:00 AM - 6:00 PM", 3);
INSERT INTO foodapp.RESTAURANT VALUES(4, "Origami 4", "M-F 10:00 AM - 6:00 PM", 1);
INSERT INTO foodapp.RESTAURANT VALUES(5, "Origami 5", "M-F 10:00 AM - 6:00 PM", 1);
INSERT INTO foodapp.RESTAURANT VALUES(6, "Qdoba 1", "M-F 10:00 AM - 6:00 PM", 2);
INSERT INTO foodapp.RESTAURANT VALUES(7, "Qdoba 2", "M-F 10:00 AM - 6:00 PM", 1);
INSERT INTO foodapp.RESTAURANT VALUES(8, "Qdoba 3", "M-F 10:00 AM - 6:00 PM", 7);
INSERT INTO foodapp.RESTAURANT VALUES(9, "Qdoba 4", "M-F 10:00 AM - 6:00 PM", 6);
INSERT INTO foodapp.RESTAURANT VALUES(10, "Qdoba 5", "M-F 10:00 AM - 6:00 PM", 1);
INSERT INTO foodapp.RESTAURANT VALUES(11, "Fire Grill 1", "M-F 10:00 AM - 6:00 PM", 3);
INSERT INTO foodapp.RESTAURANT VALUES(12, "Fire Grill 2", "M-F 10:00 AM - 6:00 PM", 8);
INSERT INTO foodapp.RESTAURANT VALUES(13, "Fire Grill 3", "M-F 10:00 AM - 6:00 PM", 1);
INSERT INTO foodapp.RESTAURANT VALUES(14, "Fire Grill 4", "M-F 10:00 AM - 6:00 PM", 10);
INSERT INTO foodapp.RESTAURANT VALUES(15, "Fire Grill 5", "M-F 10:00 AM - 6:00 PM", 19);
INSERT INTO foodapp.RESTAURANT VALUES(16, "Chick Fil A 1", "M-F 10:00 AM - 6:00 PM", 1);
INSERT INTO foodapp.RESTAURANT VALUES(17, "Chick Fil A 2", "M-F 10:00 AM - 6:00 PM", 20);
INSERT INTO foodapp.RESTAURANT VALUES(18, "Chick Fil A 3", "M-F 10:00 AM - 6:00 PM", 13);
INSERT INTO foodapp.RESTAURANT VALUES(19, "Chick Fil A 4", "M-F 10:00 AM - 6:00 PM", 15);
INSERT INTO foodapp.RESTAURANT VALUES(20, "Chick Fil A 5", "M-F 10:00 AM - 6:00 PM", 12);

SELECT * FROM foodapp.RESTAURANT;

-- not a major table, would theoretically be populated by ui, populated just in case
DROP TABLE IF EXISTS  foodapp.EMPLOYEE ;
CREATE TABLE IF NOT EXISTS foodapp.EMPLOYEE 
(
	Employee_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    FName VARCHAR(100),
    MName VARCHAR(100),
    LName VARCHAR(100),
    Days_Worked INT,
    Hours_Worked INT,
    Restaurant_ID INT NOT NULL,
    FOREIGN KEY (Restaurant_ID) REFERENCES foodapp.RESTAURANT (Restaurant_ID)
) ENGINE=InnoDB AUTO_INCREMENT= 4001 ;

INSERT INTO foodapp.EMPLOYEE VALUES(1, "John", "M", "Doe", 5, 40, 12);
INSERT INTO foodapp.EMPLOYEE VALUES(2, "John", "M", "Doe", 4, 32, 1);
INSERT INTO foodapp.EMPLOYEE VALUES(3, "John", "M", "Doe", 3, 28, 2);
INSERT INTO foodapp.EMPLOYEE VALUES(4, "John", "M", "Doe", 2, 22, 3);
INSERT INTO foodapp.EMPLOYEE VALUES(5, "John", "M", "Doe", 1, 8, 7);
INSERT INTO foodapp.EMPLOYEE VALUES(6, "John", "M", "Doe", 5, 40, 9);
INSERT INTO foodapp.EMPLOYEE VALUES(7, "John", "M", "Doe", 4, 32, 13);
INSERT INTO foodapp.EMPLOYEE VALUES(8, "John", "M", "Doe", 3, 24, 14);
INSERT INTO foodapp.EMPLOYEE VALUES(9, "John", "M", "Doe", 3, 24, 15);
INSERT INTO foodapp.EMPLOYEE VALUES(10, "John", "M", "Doe", 3, 1, 6);
INSERT INTO foodapp.EMPLOYEE VALUES(11, "John", "M", "Doe", 1, 5, 7);
INSERT INTO foodapp.EMPLOYEE VALUES(12, "John", "M", "Doe", 1, 5, 7);
INSERT INTO foodapp.EMPLOYEE VALUES(13, "John", "M", "Doe", 1, 6, 7);
INSERT INTO foodapp.EMPLOYEE VALUES(14, "John", "M", "Doe", 2, 12, 8);
INSERT INTO foodapp.EMPLOYEE VALUES(15, "John", "M", "Doe", 2, 12, 3);
INSERT INTO foodapp.EMPLOYEE VALUES(16, "John", "M", "Doe", 2, 3, 2);
INSERT INTO foodapp.EMPLOYEE VALUES(17, "John", "M", "Doe", 2, 3, 1);
INSERT INTO foodapp.EMPLOYEE VALUES(18, "John", "M", "Doe", 7, 5, 2);
INSERT INTO foodapp.EMPLOYEE VALUES(19, "John", "M", "Doe", 7, 4, 1);
INSERT INTO foodapp.EMPLOYEE VALUES(20, "John", "M", "Doe", 4, 4, 1);

SELECT * FROM foodapp.EMPLOYEE;

-- not a major table, would theoretically be populated by ui, populated just in case
DROP TABLE IF EXISTS  foodapp.FOOD_ITEM ;
CREATE TABLE IF NOT EXISTS foodapp.FOOD_ITEM 
(
	FoodItem_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    AvgGrade DECIMAL,
    Protein INT,
    Fat INT,
    Carbs INT,
    Calories INT,
    Restaurant_ID INT NOT NULL,
    FOREIGN KEY (Restaurant_ID) REFERENCES foodapp.RESTAURANT (Restaurant_ID)
    
    
) ENGINE=InnoDB AUTO_INCREMENT= 5001 ;

INSERT INTO foodapp.FOOD_ITEM VALUES(1, 5.6, 12, 14, 32, 1000, 2);
INSERT INTO foodapp.FOOD_ITEM VALUES(2, 5.7, 12, 14, 32, 1000, 1);
INSERT INTO foodapp.FOOD_ITEM VALUES(3, 5.8, 12, 14, 32, 1000, 2);
INSERT INTO foodapp.FOOD_ITEM VALUES(4, 5.9, 12, 14, 32, 1000, 3);
INSERT INTO foodapp.FOOD_ITEM VALUES(5, 4.6, 12, 14, 32, 1000, 6);
INSERT INTO foodapp.FOOD_ITEM VALUES(6, 3.6, 12, 14, 32, 1000, 7);
INSERT INTO foodapp.FOOD_ITEM VALUES(7, 2.6, 12, 14, 32, 1000, 7);
INSERT INTO foodapp.FOOD_ITEM VALUES(8, 1.6, 12, 14, 32, 1000, 7);
INSERT INTO foodapp.FOOD_ITEM VALUES(9, 5.6, 12, 14, 32, 1000, 9);
INSERT INTO foodapp.FOOD_ITEM VALUES(10, 9.8, 12, 14, 32, 1000, 9);
INSERT INTO foodapp.FOOD_ITEM VALUES(11, 5.61, 12, 14, 32, 1000, 9);
INSERT INTO foodapp.FOOD_ITEM VALUES(12, 5.62, 12, 14, 32, 1000, 9);
INSERT INTO foodapp.FOOD_ITEM VALUES(13, 5.63, 12, 14, 32, 1000, 2);
INSERT INTO foodapp.FOOD_ITEM VALUES(14, 5.64, 12, 14, 32, 1000, 2);
INSERT INTO foodapp.FOOD_ITEM VALUES(15, 5.65, 12, 14, 32, 1000, 2);
INSERT INTO foodapp.FOOD_ITEM VALUES(16, 1.6, 12, 14, 32, 1000, 5);
INSERT INTO foodapp.FOOD_ITEM VALUES(17, 2.6, 12, 14, 32, 1000, 5);
INSERT INTO foodapp.FOOD_ITEM VALUES(18, 1.6, 12, 14, 32, 1000, 5);
INSERT INTO foodapp.FOOD_ITEM VALUES(19, 4.6, 12, 14, 32, 1000, 5);
INSERT INTO foodapp.FOOD_ITEM VALUES(20, 5.6, 12, 14, 32, 1000, 5);

SELECT * FROM foodapp.FOOD_ITEM;

-- not a major table, would theoretically be populated by ui, populated just in case
DROP TABLE IF EXISTS  foodapp.STUDENT ;
CREATE TABLE IF NOT EXISTS foodapp.STUDENT 
(
	PID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Grade VARCHAR(100),
    TotalMoney INT,
    DiningPlanID  INT NOT NULL,
    FOREIGN KEY (DiningPlanID) REFERENCES foodapp.DINING_PLAN (DiningPlanID),
    FName VARCHAR(100),
    MName VARCHAR(100),
    LName VARCHAR(100)
) ENGINE=InnoDB AUTO_INCREMENT= 7001 ;

INSERT INTO foodapp.STUDENT VALUES(1, "Freshman", 200, 3, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(2, "Sophomore", 200, 4, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(3, "Junior", 200, 1, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(4, "Senior", 200, 6, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(5, "Freshman", 200, 13, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(6, "Sophomore", 200, 2, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(7, "Freshman", 200, 20, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(8, "Sophomore", 200, 5, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(9, "Freshman", 200, 1, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(10, "Sophomore", 200, 8, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(11, "Sophomore", 200, 9, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(12, "Freshman", 200, 13, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(13, "Junior", 200, 14, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(14, "Freshman", 200, 15, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(15, "Junior", 200, 6, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(16, "Freshman", 200, 9, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(17, "Freshman", 200, 4, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(18, "Junior", 200, 3, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(19, "Junior", 200, 2, "Zach", "Matthew", "Amados");
INSERT INTO foodapp.STUDENT VALUES(20, "Junior", 200, 3, "Zach", "Matthew", "Amados");

SELECT * FROM foodapp.STUDENT;

-- not a major table, would theoretically be populated by ui, populated just in case
DROP TABLE IF EXISTS  foodapp.FORUM ;
CREATE TABLE IF NOT EXISTS foodapp.FORUM 
(
    ForumID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    PID INT NOT NULL,
    FOREIGN KEY (PID) REFERENCES foodapp.STUDENT (PID),
    FoodItemID INT NOT NULL,
    FOREIGN KEY (FoodItemID) REFERENCES foodapp.FOOD_ITEM (FoodItem_ID),
    Content VARCHAR(500),
    Stars INT
) ENGINE=InnoDB AUTO_INCREMENT= 6001 ;

INSERT INTO foodapp.FORUM VALUES(1, 1, 1, "happy content 1", 3);
INSERT INTO foodapp.FORUM VALUES(2, 2, 1, "happy content 2", 1);
INSERT INTO foodapp.FORUM VALUES(3, 2, 15, "happy content 3", 2);
INSERT INTO foodapp.FORUM VALUES(4, 2, 15, "happy content 4", 3);
INSERT INTO foodapp.FORUM VALUES(5, 16, 15, "happy content 5", 2);
INSERT INTO foodapp.FORUM VALUES(6, 17, 15, "happy content 6", 2);
INSERT INTO foodapp.FORUM VALUES(7, 17, 13, "happy content 7", 2);
INSERT INTO foodapp.FORUM VALUES(8, 17, 13, "happy content 8", 2);
INSERT INTO foodapp.FORUM VALUES(9, 13, 13, "happy content 9", 5);
INSERT INTO foodapp.FORUM VALUES(10, 13, 11, "happy content 10", 4);
INSERT INTO foodapp.FORUM VALUES(11, 13, 11, "happy content 11", 4);
INSERT INTO foodapp.FORUM VALUES(12, 13, 19, "happy content 12", 4);
INSERT INTO foodapp.FORUM VALUES(13, 11, 19, "happy content 13", 5);
INSERT INTO foodapp.FORUM VALUES(14, 11, 19, "happy content 14", 5);
INSERT INTO foodapp.FORUM VALUES(15, 11, 19, "happy content 15", 5);
INSERT INTO foodapp.FORUM VALUES(16, 10, 14, "happy content 16", 1);
INSERT INTO foodapp.FORUM VALUES(17, 10, 14, "happy content 17", 1);
INSERT INTO foodapp.FORUM VALUES(18, 10, 1, "happy content 18", 3);
INSERT INTO foodapp.FORUM VALUES(19, 10, 1, "happy content 19", 3);
INSERT INTO foodapp.FORUM VALUES(20, 1, 1, "happy content 20", 3);

SELECT * FROM foodapp.FORUM;

-- not a major table, would theoretically be populated by ui, populated just in case
DROP TABLE IF EXISTS  foodapp.DH_ORDER ;
CREATE TABLE IF NOT EXISTS foodapp.DH_ORDER 
(
	OrderID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Cost INT,
    Restaurant_ID INT NOT NULL,
	FOREIGN KEY (Restaurant_ID) REFERENCES foodapp.RESTAURANT (Restaurant_ID),
	PID INT NOT NULL,
    FOREIGN KEY (PID) REFERENCES foodapp.STUDENT (PID)
) ENGINE=InnoDB AUTO_INCREMENT= 8001 ;

INSERT INTO foodapp.DH_ORDER VALUES(1, 12, 3, 2);
INSERT INTO foodapp.DH_ORDER VALUES(2, 1, 13, 1);
INSERT INTO foodapp.DH_ORDER VALUES(3, 2, 6, 6);
INSERT INTO foodapp.DH_ORDER VALUES(4, 13, 16, 4);
INSERT INTO foodapp.DH_ORDER VALUES(5, 14, 4, 2);
INSERT INTO foodapp.DH_ORDER VALUES(6, 15, 14, 8);
INSERT INTO foodapp.DH_ORDER VALUES(7, 1, 7, 8);
INSERT INTO foodapp.DH_ORDER VALUES(8, 10, 3, 8);
INSERT INTO foodapp.DH_ORDER VALUES(9, 2, 8, 2);
INSERT INTO foodapp.DH_ORDER VALUES(10, 2, 2, 2);
INSERT INTO foodapp.DH_ORDER VALUES(11, 2, 3, 2);
INSERT INTO foodapp.DH_ORDER VALUES(12, 2, 13, 4);
INSERT INTO foodapp.DH_ORDER VALUES(13, 1, 13, 4);
INSERT INTO foodapp.DH_ORDER VALUES(14, 1, 13, 4);
INSERT INTO foodapp.DH_ORDER VALUES(15, 1, 15, 9);
INSERT INTO foodapp.DH_ORDER VALUES(16, 1, 15, 9);
INSERT INTO foodapp.DH_ORDER VALUES(17, 5, 15, 9);
INSERT INTO foodapp.DH_ORDER VALUES(18, 7, 9, 2);
INSERT INTO foodapp.DH_ORDER VALUES(19, 2, 5, 2);
INSERT INTO foodapp.DH_ORDER VALUES(20, 2, 5, 2);

SELECT * FROM foodapp.DH_ORDER;


-- EXAMPLE WITH FK --------------------------------------------------
-- DROP TABLE IF EXISTS  sj8016.address ;
-- CREATE TABLE IF NOT EXISTS sj8016.address (
--          address_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
--          street VARCHAR(50) NOT NULL,
--          zip_id INTEGER,
-- 			FOREIGN KEY (zip_id)  REFERENCES sj8016.zip (zip_id)
-- 			ON UPDATE RESTRICT ON DELETE RESTRICT  
-- ) ENGINE=InnoDB AUTO_INCREMENT=5001 ;